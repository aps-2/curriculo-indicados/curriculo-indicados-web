import { Form, Input } from 'antd'
import React, { useEffect, useState } from 'react'
import { useDebounce } from '../hooks/useDebounce'

interface IFilterInput<T> {
  filterName: string
  columnName: T
  setFilters: (filter: T, value?: string) => void
}

export function FilterInput<T>(props: IFilterInput<T>) {
  const { columnName, setFilters, filterName } = props
  const [value, setValue] = useState()
  const debouncedValue = useDebounce<string>(value, 500)

  useEffect(() => {
    setFilters(columnName, debouncedValue)
  }, [debouncedValue, columnName, setFilters])

  return (
    <Form.Item label={`Filtre por ${filterName}`} style={{ padding: 10 }}>
      <Input
        allowClear
        placeholder={`Digite um ${filterName}`}
        value={value}
        onChange={e => setValue(e.target.value)}
        style={{ minWidth: 280 }}
      />
    </Form.Item>
  )
}
