import { Form, Select } from 'antd'
import React, { useEffect, useState } from 'react'
import { useDebounce } from '../hooks/useDebounce'

interface IFilterSelect<T> {
  options: string[]
  filterName: string
  columnName: T
  setFilters: (filter: T, value?: string) => void
}

export function FilterSelect<T>(props: IFilterSelect<T>) {
  const { options, filterName, columnName, setFilters } = props
  const [value, setValue] = useState()
  const debouncedValue = useDebounce<string>(value, 100)

  useEffect(() => {
    setFilters(columnName, debouncedValue)
  }, [debouncedValue, columnName, setFilters])

  return (
    <Form.Item label={`Filtre por ${filterName}`} style={{ padding: 10 }}>
      <Select
        placeholder={`Selecione um ${filterName}`}
        allowClear
        value={value}
        style={{ minWidth: 280 }}
        onChange={(value: string) => setValue(value)}
      >
        {options.map((option, index) => (
          <Select.Option key={index} value={option}>
            {option.charAt(0).toUpperCase() + option.slice(1)}
          </Select.Option>
        ))}
      </Select>
    </Form.Item>
  )
}
