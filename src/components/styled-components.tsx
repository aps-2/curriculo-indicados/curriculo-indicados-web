import styled from 'styled-components'

// Global

export const PageHeader = styled.div`
  z-index: 1;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: white;

  border-bottom: 12px solid #0084f4;
  box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.25);
`

export const PageHeaderTitle = styled.div`
  font-family: 'Raleway Medium';
  font-size: 26px;
  padding: 14px 40px;
`

export const Panel = styled.div`
  margin: 14px;
  background-color: white;
  border: 1px solid rgba(0, 0, 0, 0.05);
  box-shadow: -10px -10px 10px rgba(255, 255, 255, 0.5),
    10px 10px 10px rgba(0, 0, 0, 0.03);
`

export const PanelTitle = styled.div`
  padding: 14px;
  display: flex;
  justify-content: space-between;
  align-items: center;

  font-size: 20px;
  font-family: 'Raleway Medium';
  border-bottom: 2px solid rgba(0, 0, 0, 0.05);
`
export const PanelBody = styled.div<IPanelBodyProps>`
  padding: ${props => props.padding || '14px'};
`

interface IPanelBodyProps {
  padding?: string
}
