import React, { FunctionComponent } from 'react'
import { Empty as Vazio } from 'antd'
import { Panel, PanelTitle } from './styled-components'

interface IEmptyProps {
  name: string
}

export const Empty: FunctionComponent<IEmptyProps> = ({ name }) => {
  return (
    <Panel>
      <PanelTitle>Detalhes</PanelTitle>
      <Vazio
        description={`Selecione um ${name.toLowerCase()} para exibir detalhes`}
        image={Vazio.PRESENTED_IMAGE_SIMPLE}
      ></Vazio>
    </Panel>
  )
}
