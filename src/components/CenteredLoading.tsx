import { Spin } from 'antd'
import React, { FunctionComponent } from 'react'

export const CenteredLoading: FunctionComponent = () => (
  <div
    style={{
      display: 'flex',
      minHeight: '50%',
      justifyContent: 'center',
      alignItems: 'flex-end'
    }}
  >
    <Spin size='large' />
  </div>
)
