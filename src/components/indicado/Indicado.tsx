import { Avatar, Button, Card, Col, Icon, Row, Typography, Badge } from 'antd'
import { observer } from 'mobx-react'
import React, { FunctionComponent, useContext, useState } from 'react'
import { AuthStoreContext } from '../../stores/AuthStore'
import './indicado.scss'

const { Text } = Typography

interface IIndicadoList {
  nome: string
  cargo?: string
  local?: string
  atribuicao?: string
}

export const IndicadoList: FunctionComponent<IIndicadoList> = observer(
  ({ nome, cargo, local, atribuicao }) => {
    const [alreadyVoted, setAlreadyVoted] = useState(false)
    const [votesCount, setVotesCount] = useState(0)
    const { isAuth } = useContext(AuthStoreContext)

    const showVote = isAuth && !alreadyVoted

    function votar(increase: boolean) {
      setAlreadyVoted(true)
      setVotesCount(increase ? votesCount + 1 : votesCount - 1)
    }

    return (
      <div style={{ margin: 14 }}>
        <Badge count={votesCount}>
          <Card bordered={false} id='card'>
            <Row
              type='flex'
              justify='center'
              align='middle'
              style={{ minWidth: 350 }}
            >
              <Col span={8} id='avatar'>
                <Avatar
                  size={106}
                  src={require('../../assets/images/card-img.png')}
                  style={{ marginRight: 14 }}
                />
              </Col>
              <Col span={16}>
                <Row>
                  <Text strong>Nome: {nome}</Text>
                </Row>
                <Row>
                  <Text strong>
                    Cargo: {cargo} - {local}{' '}
                  </Text>
                </Row>
                <Row>
                  <Text strong>
                    Atribuição:{' '}
                    {atribuicao?.slice(0, 1).toUpperCase()! +
                      atribuicao?.slice(1)}
                  </Text>
                </Row>
                <hr />
                {showVote && (
                  <Button id='buttonGreen' onClick={() => votar(true)}>
                    Sim{' '}
                    <Icon type='like' theme='twoTone' twoToneColor='#52c41a' />
                  </Button>
                )}
                {showVote && (
                  <Button id='buttonRed' onClick={() => votar(false)}>
                    Não{' '}
                    <Icon
                      type='dislike'
                      theme='twoTone'
                      twoToneColor='#c21f19'
                    />
                  </Button>
                )}
              </Col>
            </Row>
          </Card>
        </Badge>
      </div>
    )
  }
)
