import { Button, Form, Icon, Input, Modal } from 'antd'
import { useFormik } from 'formik'
import React, { FunctionComponent, useContext } from 'react'
import yup from '../../../../shared/yupPtBrLocale'
import { AuthStoreContext } from '../../../../stores/AuthStore'

interface ISigninModal {
  state: boolean
  setSignInModalState: React.Dispatch<React.SetStateAction<boolean>>
}

export const SignInModal: FunctionComponent<ISigninModal> = ({
  state,
  setSignInModalState
}) => {
  const { Item } = Form
  const { signup } = useContext(AuthStoreContext)

  const {
    handleChange,
    handleBlur,
    handleSubmit,
    errors,
    touched,
    values,
    isValid,
    isSubmitting
  } = useFormik({
    initialValues: {
      name: '',
      email: '',
      password: '',
      confirmpw: ''
    },
    onSubmit: async ({ name, email, password }) => {
      try {
        await signup({ name, email, password })
        setSignInModalState(false)
      } catch {
        setSignInModalState(true)
      }
    },
    validationSchema: loginValidationSchema
  })

  return (
    <Modal
      title='Cadastro'
      visible={state}
      onCancel={() => setSignInModalState(false)}
      footer={null}
      width={300}
    >
      <Form onSubmit={handleSubmit}>
        <Item
          validateStatus={errors.name && touched.name ? 'error' : 'success'}
          help={touched.name && errors.name}
        >
          <Input
            id='name'
            name='name'
            prefix={<Icon type='user' style={{ color: 'rgba(0,0,0,.25)' }} />}
            placeholder='Nome'
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.name}
          ></Input>
        </Item>

        <Item
          validateStatus={errors.email && touched.email ? 'error' : 'success'}
          help={touched.email && errors.email}
        >
          <Input
            id='email'
            name='email'
            prefix={<Icon type='mail' style={{ color: 'rgba(0,0,0,.25)' }} />}
            placeholder='Email'
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.email}
          ></Input>
        </Item>

        <Item
          validateStatus={
            errors.password && touched.password ? 'error' : 'success'
          }
          help={touched.password && errors.password}
        >
          <Input
            id='password'
            name='password'
            type='password'
            prefix={<Icon type='key' style={{ color: 'rgba(0,0,0,.25)' }} />}
            placeholder='Senha'
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.password}
          ></Input>
        </Item>

        <Item
          validateStatus={
            values.password !== values.confirmpw ? 'error' : 'success'
          }
          help={values.password !== values.confirmpw && 'Não coincide'}
        >
          <Input
            id='confirmpw'
            name='confirmpw'
            type='password'
            prefix={<Icon type='key' style={{ color: 'rgba(0,0,0,.25)' }} />}
            placeholder='Confirmação de Senha'
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.confirmpw}
          ></Input>
        </Item>

        <Item>
          <Button
            loading={isSubmitting}
            disabled={!isValid || values.password !== values.confirmpw}
            type='primary'
            htmlType='submit'
            style={{ width: '100%' }}
          >
            Entrar
          </Button>
        </Item>
      </Form>
    </Modal>
  )
}

const loginValidationSchema = yup.object().shape({
  name: yup.string().required(),
  email: yup
    .string()
    .email()
    .required(),
  password: yup
    .string()
    .required()
    .min(8)
})
