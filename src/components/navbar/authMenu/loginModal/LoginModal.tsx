import { Button, Form, Icon, Input, Modal } from 'antd'
import { useFormik } from 'formik'
import React, { FunctionComponent, useContext } from 'react'
import yup from '../../../../shared/yupPtBrLocale'
import { AuthStoreContext } from '../../../../stores/AuthStore'

interface ILoginModal {
  state: boolean
  setLoginModalState: React.Dispatch<React.SetStateAction<boolean>>
}

export const LoginModal: FunctionComponent<ILoginModal> = ({
  state,
  setLoginModalState
}) => {
  const { login } = useContext(AuthStoreContext)

  const {
    handleChange,
    handleBlur,
    handleSubmit,
    errors,
    touched,
    values,
    isValid,
    isSubmitting
  } = useFormik({
    initialValues: {
      email: '',
      password: ''
    },
    onSubmit: async ({ email, password }) => {
      try {
        await login({ email, password })
        setLoginModalState(false)
      } catch {
        setLoginModalState(true)
      }
    },
    validationSchema: loginValidationSchema
  })

  return (
    <Modal
      title='Login'
      visible={state}
      onCancel={() => setLoginModalState(false)}
      footer={null}
      width={300}
    >
      <Form onSubmit={handleSubmit}>
        <Form.Item
          validateStatus={errors.email && touched.email ? 'error' : 'success'}
          help={touched.email && errors.email}
        >
          <Input
            id='email'
            name='email'
            prefix={<Icon type='mail' style={{ color: 'rgba(0,0,0,.25)' }} />}
            placeholder='Email'
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.email}
          ></Input>
        </Form.Item>
        <Form.Item
          validateStatus={
            errors.password && touched.password ? 'error' : 'success'
          }
          help={touched.password && errors.password}
        >
          <Input
            id='password'
            name='password'
            type='password'
            prefix={<Icon type='key' style={{ color: 'rgba(0,0,0,.25)' }} />}
            placeholder='Senha'
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.password}
          ></Input>
        </Form.Item>
        <Form.Item>
          <Button
            loading={isSubmitting}
            disabled={!isValid}
            type='primary'
            htmlType='submit'
            style={{ width: '100%' }}
          >
            Entrar
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  )
}

const loginValidationSchema = yup.object().shape({
  email: yup
    .string()
    .email()
    .required(),
  password: yup.string().required()
})
