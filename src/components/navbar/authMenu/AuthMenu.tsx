import { Divider } from 'antd'
import { observer } from 'mobx-react'
import React, { FunctionComponent, useContext, useState } from 'react'
import { AuthStoreContext } from '../../../stores/AuthStore'
import { AuthDropdown } from '../authDropdown/AuthDropdown'
import { LoginModal } from './loginModal/LoginModal'
import { SignInModal } from './signinModal/SignInModal'
import styled from 'styled-components'

export const AuthMenu: FunctionComponent = observer(() => {
  const { isAuth } = useContext(AuthStoreContext)
  const [loginModalState, setLoginModalState] = useState(false)
  const [signinModalState, setSignInModalState] = useState(false)

  if (isAuth) return <AuthDropdown />

  return (
    <AuthOptions id='auth'>
      <Anchor onClick={() => setSignInModalState(true)}>Cadastre-se</Anchor>
      <Divider type='vertical' />
      <Anchor onClick={() => setLoginModalState(true)}>Login</Anchor>

      {/* Modais */}
      <LoginModal
        state={loginModalState}
        setLoginModalState={setLoginModalState}
      />
      <SignInModal
        state={signinModalState}
        setSignInModalState={setSignInModalState}
      />
    </AuthOptions>
  )
})

const AuthOptions = styled.div`
  flex-grow: 1;

  display: flex;
  justify-content: space-around;
  align-items: center;

  font-family: 'Raleway Bold';
  font-size: 14px;
`
const Anchor = styled.a``
