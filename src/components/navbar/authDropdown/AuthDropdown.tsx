import { Avatar, Dropdown, Icon, Menu } from 'antd'
import React, { FunctionComponent, useContext } from 'react'
import { Link } from 'react-router-dom'
import { AuthStoreContext } from '../../../stores/AuthStore'

export const AuthDropdown: FunctionComponent = () => {
  const { logedUserName, logedUserAvatar, logoff, isUser } = useContext(
    AuthStoreContext
  )
  const { Item } = Menu

  const menu = (
    <Menu>
      {!isUser && (
        <Item>
          <Link to='/usuarios'>Usuários</Link>
        </Item>
      )}
      {!isUser && (
        <Item>
          <Link to='/indicados'> Indicados </Link>
        </Item>
      )}
      <Item onClick={() => logoff()}>Sair</Item>
    </Menu>
  )

  return (
    <Dropdown overlay={menu}>
      <div>
        <Avatar src={logedUserAvatar} style={{ marginRight: 14 }} />
        {logedUserName}
        <Icon type='down' style={{ marginLeft: 4 }} />
      </div>
    </Dropdown>
  )
}
