import { Divider } from 'antd'
import { observer } from 'mobx-react'
import React, { FunctionComponent, useContext } from 'react'
import { Link } from 'react-router-dom'
import { NavigationStoreContext } from '../../stores/NavigationStore'
import { AuthMenu } from './authMenu/AuthMenu'
import styled from 'styled-components'

export const Navbar: FunctionComponent = observer(() => {
  const { currentRoute } = useContext(NavigationStoreContext)

  return (
    <NavigationBar>
      <Logo>
        <Link to='/'>CURRÍCULO DE INDICADOS</Link>
      </Logo>
      <NavigationOptions>
        <Link className={currentRoute === 'sobre' ? 'active' : ''} to='/sobre'>
          QUEM SOMOS
        </Link>
        <span>RANKING</span>
        <span>PARTICIPE</span>
      </NavigationOptions>
      <Divider type='vertical' style={{ margin: '0 40px' }} />
      <AuthMenu />
    </NavigationBar>
  )
})

const NavigationBar = styled.nav`
  z-index: 2;
  width: 100%;
  height: 60px;
  padding: 20px 40px;
  position: relative;

  display: flex;
  flex-flow: row nowrap;
  align-items: center;

  color: white;
  background-color: #0084f4;
  box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.25);

  a {
    color: inherit;
  }
  a.active {
    border-bottom: 2px solid white;
  }
  a:hover {
    color: #40a9ff;
  }
`
const Logo = styled.div`
  flex-grow: 10;
  font-family: 'Poppins Black';
  font-size: 22px;
`

const NavigationOptions = styled.div`
  flex-grow: 1;

  display: flex;
  justify-content: space-around;

  font-family: 'Raleway Bold';
  font-size: 14px;
`
