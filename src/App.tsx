import { observer } from 'mobx-react'
import React, { FunctionComponent, useContext } from 'react'
import Helmet from 'react-helmet'
import { Navbar } from './components/navbar/Navbar'
import { Routes } from './routes'
import { AuthStoreContext } from './stores/AuthStore'
import { NavigationStoreContext } from './stores/NavigationStore'

const App: FunctionComponent = observer(() => {
  const { pageTitle } = useContext(NavigationStoreContext)
  const { restoreAuth } = useContext(AuthStoreContext)

  restoreAuth()

  return (
    <>
      <Helmet>
        <title>{pageTitle}</title>
      </Helmet>
      <Navbar />
      <Routes />
    </>
  )
})

export default App
