export interface IDecodedToken {
  iat: number
  exp: number
  aud: string
  iss: string
  sub: string
  jti: string
  name: string
  role: string
  email: string
  avatar: string
}
