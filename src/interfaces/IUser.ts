export type UserRole = 'admin' | 'mod' | 'user'

export interface IUserRouteParams {
  id: string
}
export interface IUser {
  id: number
  avatar: string
  name: string
  email: string
  role: UserRole
  updatedAt: string
  createdAt: string
}

export interface IUserPayload {
  id?: number
  role?: UserRole
  name?: string
  email?: string
  password?: string
}
