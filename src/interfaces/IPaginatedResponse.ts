export interface IPaginatedResponse<T> {
  total: number
  limit: number
  skip: number
  data: T[]
}
