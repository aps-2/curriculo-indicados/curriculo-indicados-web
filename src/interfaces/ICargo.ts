import { ILocal } from './ILocal'

export interface ICargo {
  id: number
  nome: string
  localId: number
  createdAt: string
  updatedAt: string
  local?: ILocal
}
