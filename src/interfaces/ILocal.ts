export interface ILocal {
  id: number
  nome: string
  createdAt: string
  updatedAt: string
}
