import { IUser } from './IUser'

export interface IAuthResponse {
  accessToken: string
  authentication: {
    strategy: string
  }
  user: IUser
}

export interface IAuthPayload {
  email: string
  password: string
}
