import { ICargo } from './ICargo'

export type IndicadoStatus = 'alternativa' | 'indicado'

export interface IIndicadoRouteParams {
  id: string
}
export interface IIndicado {
  id: number
  nome: string
  status: IndicadoStatus
  cargoId: number
  updatedAt: string
  createdAt: string
  cargo?: ICargo
}

export interface IIndicadoPayload {
  id?: number
  nome: string
  status: IndicadoStatus
  cargoId: number
}

const indicado = {} as IIndicado
