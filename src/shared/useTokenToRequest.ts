import { AxiosRequestConfig } from 'axios'
import { AuthStoreImpl } from '../stores/AuthStore'

export const useTokenToRequest = (config: AxiosRequestConfig) => {
  const { isAuth, token } = AuthStoreImpl

  if (!isAuth) return config

  config.headers.Authorization = `bearer ${token}`

  return config
}
