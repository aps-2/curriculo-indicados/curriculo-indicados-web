import { ICargo } from '../../interfaces/ICargo'
import { IPaginatedResponse } from '../../interfaces/IPaginatedResponse'
import { createApiError } from '../createApierror'
import serverClient from '../serverClient'

const cargosUrl = '/cargos'

export const tryGetCargos = async () => {
  try {
    const params = {
      $include: 'locals'
    }

    const response = await serverClient.get<IPaginatedResponse<ICargo>>(
      cargosUrl,
      { params }
    )

    return response.data
  } catch (e) {
    createApiError(e)
    throw e
  }
}
