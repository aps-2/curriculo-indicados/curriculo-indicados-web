import { IIndicado, IIndicadoPayload } from '../../interfaces/IIndicado'
import { IPaginatedResponse } from '../../interfaces/IPaginatedResponse'
import { IIndicadoFilters } from '../../stores/IndicadoStore'
import { createApiError } from '../createApierror'
import serverClient from '../serverClient'

const indicadosUrl = '/indicados'

export const tryGetIndicado = async (id: string) => {
  try {
    const response = await serverClient.get<IIndicado>(`${indicadosUrl}/${id}`)

    return response.data
  } catch (e) {
    createApiError(e)
    throw e
  }
}

export const tryGetIndicados = async (
  page: number,
  pageSize: number,
  filters: IIndicadoFilters
) => {
  try {
    const params = {
      $skip: page,
      $limit: pageSize,
      $sort: JSON.stringify({
        nome: 'ASC'
      }),
      $like: JSON.stringify({
        nome: filters.nome
      }),
      $include: 'cargos.locals',
      cargoId: filters.cargoId
    }

    const response = await serverClient.get<IPaginatedResponse<IIndicado>>(
      indicadosUrl,
      { params }
    )

    return response.data
  } catch (e) {
    createApiError(e)
    throw e
  }
}

export const tryPostIndicado = async (payload: IIndicadoPayload) => {
  try {
    const response = await serverClient.post<IIndicado>(indicadosUrl, payload)

    return response.data
  } catch (e) {
    createApiError(e)
    throw e
  }
}

export const tryPutIndicado = async (payload: IIndicadoPayload) => {
  try {
    const response = await serverClient.put<IIndicado>(
      `${indicadosUrl}/${payload.id}`,
      payload
    )

    return response.data
  } catch (e) {
    createApiError(e)
    throw e
  }
}

export const tryDeleteIndicado = async (id: string) => {
  try {
    const response = await serverClient.delete<IIndicado>(
      `${indicadosUrl}/${id}`
    )

    return response.data
  } catch (e) {
    createApiError(e)
    throw e
  }
}
