import { IPaginatedResponse } from '../../interfaces/IPaginatedResponse'
import { IUser, IUserPayload } from '../../interfaces/IUser'
import { createApiError } from '../createApierror'
import serverClient from '../serverClient'
import { IUserFilters } from '../../stores/UserStore'

const usersUrl = '/users'

export const tryPostSignUp = async (payload: IUserPayload) => {
  try {
    const response = await serverClient.post<IUser>(usersUrl, payload)

    return response.data
  } catch (e) {
    createApiError(e)
    throw e
  }
}

export const tryGetUser = async (id: string) => {
  try {
    const response = await serverClient.get<IUser>(`${usersUrl}/${id}`)

    return response.data
  } catch (e) {
    createApiError(e)
    throw e
  }
}

export const tryGetUsers = async (
  page: number,
  pageSize: number,
  filters: IUserFilters
) => {
  try {
    const params = {
      $skip: page,
      $limit: pageSize,
      $sort: JSON.stringify({
        name: 'ASC',
        email: 'ASC'
      }),
      $like: JSON.stringify({
        name: filters.name,
        email: filters.email
      }),
      role: filters.role
    }

    const response = await serverClient.get<IPaginatedResponse<IUser>>(
      usersUrl,
      { params }
    )

    return response.data
  } catch (e) {
    createApiError(e)
    throw e
  }
}

export const tryPostUser = async (payload: IUserPayload) => {
  try {
    const response = await serverClient.post<IUser>(usersUrl, payload)

    return response.data
  } catch (e) {
    createApiError(e)
    throw e
  }
}

export const tryPatchUser = async (payload: IUserPayload) => {
  try {
    const response = await serverClient.patch<IUser>(
      `${usersUrl}/${payload.id}`,
      payload
    )

    return response.data
  } catch (e) {
    createApiError(e)
    throw e
  }
}

export const tryDeleteUser = async (id: string) => {
  try {
    const response = await serverClient.delete<IUser>(`${usersUrl}/${id}`)

    return response.data
  } catch (e) {
    createApiError(e)
    throw e
  }
}
