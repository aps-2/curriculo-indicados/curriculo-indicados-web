import { IAuthPayload, IAuthResponse } from '../../interfaces/IAuth'
import { createApiError } from '../createApierror'
import serverClient from '../serverClient'

const authUrl = '/auth'

export const tryPostAuth = async (payload: IAuthPayload) => {
  try {
    const response = await serverClient.post<IAuthResponse>(authUrl, {
      strategy: 'local',
      ...payload
    })

    return response.data
  } catch (e) {
    createApiError(e)
    throw e
  }
}
