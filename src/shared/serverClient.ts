import Axios from 'axios'
import { stringify } from 'querystring'
import { useTokenToRequest } from './useTokenToRequest'

console.info(
  'process.env: (actual enviroment) \n' + JSON.stringify(process.env, null, 2)
)

const serverClient = Axios.create({
  baseURL: process.env.REACT_APP_BASE_URL
})
serverClient.defaults.paramsSerializer = params => stringify(params)
serverClient.interceptors.request.use(useTokenToRequest)

export default serverClient
