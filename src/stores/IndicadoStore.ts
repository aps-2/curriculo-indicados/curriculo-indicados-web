import { message } from 'antd'
import { PaginationConfig } from 'antd/lib/table'
import { action, computed, observable } from 'mobx'
import { createContext } from 'react'
import { IIndicado, IIndicadoPayload } from '../interfaces/IIndicado'
import {
  tryGetIndicado,
  tryGetIndicados,
  tryPostIndicado,
  tryPutIndicado,
  tryDeleteIndicado
} from '../shared/serverApi/indicadoApi'

export interface IIndicadoFilters {
  nome?: string
  cargoId?: number
  status?: string
}

class IndicadoStore {
  // Estado
  @observable indicado: IIndicado
  @observable indicados: IIndicado[]
  @observable isLoadingIndicado: boolean
  @observable isLoadingIndicados: boolean

  // Paginação e Filtros
  @observable total: number
  @observable page: number = 0
  @observable pageSize: number = 10
  @observable filters: IIndicadoFilters = {}

  @action.bound setPage = (page: number) => {
    this.page = (page - 1) * this.pageSize
  }

  @action.bound setPageSize = (size: number) => {
    this.pageSize = size
  }

  @action.bound setFilters = (
    filter: keyof IIndicadoFilters,
    value?: string
  ) => {
    this.filters = {
      ...this.filters,
      [filter]: value
    }
  }

  @computed get pageConfig(): PaginationConfig {
    return {
      total: this.total,
      current: this.page,
      pageSize: this.pageSize,
      showSizeChanger: true,
      onChange: (nextPage, nextPageSize) => {
        this.setPage(nextPage)
        if (nextPageSize) this.setPageSize(nextPageSize)
      },
      onShowSizeChange: (_, size) => {
        this.setPageSize(size)
      }
    }
  }

  // Actions

  @action.bound fetchIndicado = async (id: string) => {
    try {
      this.isLoadingIndicado = true
      const response = await tryGetIndicado(id)

      this.indicado = { ...response }
    } finally {
      this.isLoadingIndicado = false
    }
  }

  @action.bound fetchIndicados = async () => {
    try {
      this.isLoadingIndicados = true
      const response = await tryGetIndicados(
        this.page,
        this.pageSize,
        this.filters
      )

      this.indicados = [...response.data]
      this.total = response.total
    } finally {
      this.isLoadingIndicados = false
    }
  }

  @action.bound saveIndicado = async (payload: IIndicadoPayload) => {
    try {
      const response = payload.id
        ? await tryPutIndicado(payload)
        : await tryPostIndicado(payload)

      message.success(`Indicado ${response.nome} salvo com sucesso.`)
      return response
    } finally {
    }
  }

  @action.bound deleteIndicado = async (id: string) => {
    try {
      const response = await tryDeleteIndicado(id)

      message.success(`Indicado ${response.nome} removido com sucesso.`)
      return response
    } finally {
    }
  }
}

export const IndicadoStoreContext = createContext(new IndicadoStore())
