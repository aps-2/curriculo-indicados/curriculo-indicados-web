import { message } from 'antd'
import { PaginationConfig } from 'antd/lib/table'
import { action, computed, observable } from 'mobx'
import { createContext } from 'react'
import { IUser, IUserPayload } from '../interfaces/IUser'
import {
  tryDeleteUser,
  tryGetUser,
  tryGetUsers,
  tryPatchUser,
  tryPostUser
} from '../shared/serverApi/userApi'

export interface IUserFilters {
  name?: string
  role?: string
  email?: string
}

class UserStore {
  // Estado
  @observable user: IUser
  @observable users: IUser[]
  @observable isLoadingUser: boolean
  @observable isLoadingUsers: boolean

  // Paginação e Filtros
  @observable total: number
  @observable page: number = 0
  @observable pageSize: number = 10
  @observable filters: IUserFilters = {}

  @action.bound setPage = (page: number) => {
    this.page = (page - 1) * this.pageSize
  }

  @action.bound setPageSize = (size: number) => {
    this.pageSize = size
  }

  @action.bound setFilters = (filter: keyof IUserFilters, value?: string) => {
    this.filters = {
      ...this.filters,
      [filter]: value
    }
  }

  @computed get pageConfig(): PaginationConfig {
    return {
      total: this.total,
      current: this.page,
      pageSize: this.pageSize,
      showSizeChanger: true,
      onChange: (nextPage, nextPageSize) => {
        this.setPage(nextPage)
        if (nextPageSize) this.setPageSize(nextPageSize)
      },
      onShowSizeChange: (_, size) => {
        this.setPageSize(size)
      }
    }
  }

  // Actions

  @action.bound fetchUser = async (id: string) => {
    try {
      this.isLoadingUser = true
      const response = await tryGetUser(id)

      this.user = { ...response }
    } finally {
      this.isLoadingUser = false
    }
  }

  @action.bound fetchUsers = async () => {
    try {
      this.isLoadingUsers = true
      const response = await tryGetUsers(this.page, this.pageSize, this.filters)

      this.users = [...response.data]
      this.total = response.total
    } finally {
      this.isLoadingUsers = false
    }
  }

  @action.bound saveUser = async (payload: IUserPayload) => {
    try {
      const response = payload.id
        ? await tryPatchUser(payload)
        : await tryPostUser(payload)

      message.success(`Usuário ${response.name} salvo com sucesso.`)
      return response
    } finally {
    }
  }

  @action.bound deleteUser = async (id: string) => {
    try {
      const response = await tryDeleteUser(id)

      message.success(`Usuário ${response.name} removido com sucesso.`)
      return response
    } finally {
    }
  }
}

export const UserStoreContext = createContext(new UserStore())
