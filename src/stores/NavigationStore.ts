import { observable, action } from 'mobx'
import { createContext } from 'react'

class NavigationStore {
  @observable currentRoute: string
  @observable pageTitle?: string

  @action.bound setCurrentRoute = (routeName: string, pageTitle?: string) => {
    this.currentRoute = routeName
    this.pageTitle = pageTitle ? `CI - ${pageTitle}` : 'Currículo de Indicados'
  }
}

export const NavigationStoreContext = createContext(new NavigationStore())
