import { message } from 'antd'
import jwtDecode from 'jwt-decode'
import { action, computed, observable } from 'mobx'
import moment from 'moment'
import { createContext } from 'react'
import { IAuthPayload } from '../interfaces/IAuth'
import { IDecodedToken } from '../interfaces/IDecodedToken'
import { IUserPayload } from '../interfaces/IUser'
import { tryPostAuth } from '../shared/serverApi/authApi'
import { tryPostSignUp } from '../shared/serverApi/userApi'

class AuthStore {
  @observable isAuth: boolean = false
  @observable token?: string
  @observable decodedToken?: IDecodedToken

  @computed get logedUserEmail() {
    return this.decodedToken?.email
  }

  @computed get logedUserAvatar() {
    return this.decodedToken?.avatar
  }

  @computed get logedUserName() {
    return this.decodedToken?.name
  }

  @computed get isAdmin() {
    return this.decodedToken?.role === 'admin'
  }

  @computed get isMod() {
    return this.decodedToken?.role === 'mod'
  }

  @computed get isUser() {
    return this.decodedToken?.role === 'user'
  }

  @action.bound signup = async (payload: IUserPayload) => {
    try {
      const { name: createdName } = await tryPostSignUp(payload)
      message.success(`Usuário ${createdName} registrado com sucesso.`)
    } catch (e) {
      this.logoff()
      throw e
    }
  }

  @action.bound login = async (payload: IAuthPayload) => {
    try {
      const response = await tryPostAuth(payload)
      this.isAuth = true
      this.token = response.accessToken
      this.decodedToken = jwtDecode(response.accessToken)
      localStorage.setItem('token', response.accessToken)
    } catch (e) {
      this.logoff()
      throw e
    }
  }

  @action.bound restoreAuth = () => {
    const localStoredToken = localStorage.getItem('token')

    if (!localStoredToken || !this.isValidToken(localStoredToken)) {
      return this.logoff()
    }

    this.isAuth = true
    this.token = localStoredToken
    this.decodedToken = jwtDecode(localStoredToken)
  }

  @action.bound logoff = async () => {
    this.isAuth = false
    this.token = undefined
    this.decodedToken = undefined
    localStorage.removeItem('token')
  }

  @action.bound isValidToken = (token: string) => {
    const decodedToken: IDecodedToken = jwtDecode(token)
    const now = moment()
    const tokenExpiration = moment(decodedToken.exp * 1000)

    return tokenExpiration.isAfter(now)
  }
}
export const AuthStoreImpl = new AuthStore()
export const AuthStoreContext = createContext(AuthStoreImpl)
