import { ConfigProvider, Icon, Spin } from 'antd'
import ptBR from 'antd/es/locale/pt_BR'
import moment from 'moment'
import 'moment/locale/pt-br'
import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import App from './App'
import './index.css'
import './assets/styles/holymoly.scss'
import * as serviceWorker from './serviceWorker'

const loadingIndicator = <Icon type='loading' style={{ fontSize: 24 }} spin />

async function initApp() {
  moment.locale('pt-br')
  Spin.setDefaultIndicator(loadingIndicator)

  ReactDOM.render(
    <ConfigProvider locale={ptBR}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </ConfigProvider>,
    document.getElementById('root')
  )

  serviceWorker.unregister()
}

initApp()
