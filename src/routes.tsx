import React, { lazy, Suspense } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { CenteredLoading } from './components/CenteredLoading'
import { PrivateRoute } from './components/PrivateRoute'

export const Routes: React.FunctionComponent = () => {
  return (
    <Suspense fallback={<CenteredLoading />}>
      <Switch>
        <Route
          path='/'
          exact
          component={lazy(() => import('./screens/home/Home'))}
        />
        <Route
          path='/sobre'
          component={lazy(() => import('./screens/about/About'))}
        />
        <PrivateRoute
          to={['admin', 'mod']}
          path='/usuarios'
          component={lazy(() => import('./screens/users/UserIndex'))}
        />
        <PrivateRoute
          to={['admin', 'mod']}
          path='/indicados'
          component={lazy(() => import('./screens/indicados/IndicadoIndex'))}
        />
        <Route path='*'>
          <Redirect to='/' />
        </Route>
      </Switch>
    </Suspense>
  )
}
