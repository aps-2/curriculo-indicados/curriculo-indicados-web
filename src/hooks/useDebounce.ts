import { useState, useEffect } from 'react'

export function useDebounce<T>(value: T, delay: number = 500) {
  const [debouncedValue, setDebouncedValue] = useState<T>()

  useEffect(() => {
    // Atualiza o valor depois do delay
    const handler = setTimeout(() => {
      setDebouncedValue(value)
    }, delay)

    // Cancela a atualização caso alguma depedência do efeito mude: [value]
    return () => {
      clearTimeout(handler)
    }
  }, [value, delay])

  return debouncedValue
}
