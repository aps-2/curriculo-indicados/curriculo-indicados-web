import { useContext } from 'react'
import { NavigationStoreContext } from '../stores/NavigationStore'

export const useAsPage = (routeName: string, pageTitle?: string) => {
  const { setCurrentRoute } = useContext(NavigationStoreContext)

  setCurrentRoute(routeName, pageTitle)
}
