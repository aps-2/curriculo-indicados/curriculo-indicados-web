import { Form, Select } from 'antd'
import React, { useEffect, useState } from 'react'
import { useDebounce } from '../../hooks/useDebounce'
import { ICargo } from '../../interfaces/ICargo'
import { tryGetCargos } from '../../shared/serverApi/cargosApi'
import { IIndicadoFilters } from '../../stores/IndicadoStore'

interface IFilterCargos {
  setFilters: (filter: keyof IIndicadoFilters, value?: string) => void
}

export function FilterCargos(props: IFilterCargos) {
  const { setFilters } = props
  const [value, setValue] = useState()
  const [options, setOptions] = useState([] as ICargo[])
  const [isLoading, setIsLoading] = useState(false)
  const debouncedValue = useDebounce<string>(value, 100)

  const fetchCargos = async () => {
    try {
      setIsLoading(true)
      const response = await tryGetCargos()

      setOptions(response.data)
    } finally {
      setIsLoading(false)
    }
  }

  useEffect(() => {
    setFilters('cargoId', debouncedValue)
  }, [debouncedValue, setFilters])

  return (
    <Form.Item label={`Filtre por Cargo`} style={{ padding: 10 }}>
      <Select
        loading={isLoading}
        placeholder={`Selecione um Cargo`}
        allowClear
        value={value}
        style={{ minWidth: 280 }}
        onFocus={() => fetchCargos()}
        onChange={(value: string) => setValue(value)}
      >
        {options.map((option, index) => (
          <Select.Option key={index} value={option.id}>
            {option.nome + ' - ' + option.local?.nome}
          </Select.Option>
        ))}
      </Select>
    </Form.Item>
  )
}
