import React, { FunctionComponent, useContext } from 'react'
import { useHistory } from 'react-router-dom'
import { IIndicadoPayload } from '../../interfaces/IIndicado'
import { IndicadoStoreContext } from '../../stores/IndicadoStore'
import { IndicadoForm } from './IndicadoForm'

export const IndicadoCreate: FunctionComponent = () => {
  const { saveIndicado, fetchIndicados } = useContext(IndicadoStoreContext)
  const { push } = useHistory()
  const initialValues = {} as IIndicadoPayload

  const onSaveIndicado = async (payload: IIndicadoPayload) => {
    const { id } = await saveIndicado(payload)
    await fetchIndicados()
    push(`/indicados/editando/${id}`)
  }

  return (
    <IndicadoForm
      initialValues={initialValues}
      onSaveIndicado={onSaveIndicado}
    />
  )
}