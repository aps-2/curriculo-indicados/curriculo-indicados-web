import { Select } from 'antd'
import React, { useEffect, useState } from 'react'
import { ICargo } from '../../interfaces/ICargo'
import { tryGetCargos } from '../../shared/serverApi/cargosApi'

interface ISelectCargos {
  id: string
  value: number
  setFieldTouched: (field: string, touched?: boolean) => any
  setFieldValue: (field: string, value?: number) => any
}

export function SelectCargos(props: ISelectCargos) {
  const { id, value, setFieldTouched, setFieldValue } = props
  const [cargos, setCargos] = useState([] as ICargo[])
  const [isLoading, setIsLoading] = useState(false)

  const fetchCargos = async () => {
    try {
      setIsLoading(true)
      const response = await tryGetCargos()
      setCargos(response.data)
    } finally {
      setIsLoading(false)
    }
  }

  useEffect(() => {
    fetchCargos()
  }, [])

  return (
    <Select
      id={id}
      loading={isLoading}
      placeholder={`Selecione um Cargo`}
      style={{ minWidth: 280 }}
      onFocus={() => fetchCargos()}
      onBlur={() => setFieldTouched('cargoId', true)}
      onChange={(value: number) => setFieldValue('cargoId', value)}
      value={value}
    >
      {cargos.map((cargo, index) => (
        <Select.Option key={index} value={cargo.id}>
          {cargo.nome + ' - ' + cargo.local?.nome}
        </Select.Option>
      ))}
    </Select>
  )
}
