import { Spin } from 'antd'
import { observer } from 'mobx-react'
import React, { FunctionComponent, useContext, useEffect } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { IIndicadoPayload, IIndicadoRouteParams } from '../../interfaces/IIndicado'
import { IndicadoStoreContext } from '../../stores/IndicadoStore'
import { IndicadoForm } from './IndicadoForm'

export const IndicadoEdit: FunctionComponent = observer(() => {
  const {
    indicado,
    saveIndicado,
    fetchIndicado,
    fetchIndicados,
    isLoadingIndicado,
    deleteIndicado
  } = useContext(IndicadoStoreContext)
  const { id } = useParams<IIndicadoRouteParams>()
  const { push } = useHistory()
  const initialValues = {
    id: indicado?.id,
    nome: indicado?.nome,
    status: indicado?.status,
    cargoId: indicado?.cargoId
  } as IIndicadoPayload

  const onSaveIndicado = async (payload: IIndicadoPayload) => {
    await saveIndicado(payload)
    await fetchIndicados()
  }

  const onDeleteIndicado = async () => {
    await deleteIndicado(id)
    await fetchIndicados()
    push('/indicados')
  }

  useEffect(() => {
    fetchIndicado(id)
  }, [id, fetchIndicado])

  return (
    <Spin spinning={isLoadingIndicado}>
      <IndicadoForm
        initialValues={initialValues}
        onSaveIndicado={onSaveIndicado}
        onDeleteIndicado={onDeleteIndicado}
      />
    </Spin>
  )
})
