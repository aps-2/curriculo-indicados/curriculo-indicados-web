import { Button, Col, Form, Icon, Input, Row, Select } from 'antd'
import { useFormik } from 'formik'
import React, { FunctionComponent } from 'react'
import { Panel, PanelBody, PanelTitle } from '../../components/styled-components'
import { IIndicadoPayload, IndicadoStatus } from '../../interfaces/IIndicado'
import yup from '../../shared/yupPtBrLocale'
import { SelectCargos } from './SelectCargos'

interface ILoginModal {
  initialValues: IIndicadoPayload
  onSaveIndicado: (payload: IIndicadoPayload) => Promise<void>
  onDeleteIndicado?: () => Promise<void>
}

export const IndicadoForm: FunctionComponent<ILoginModal> = ({
  initialValues,
  onSaveIndicado,
  onDeleteIndicado
}) => {
  const { Item } = Form
  const { Option } = Select

  const {
    handleChange,
    handleBlur,
    handleSubmit,
    errors,
    touched,
    values,
    isSubmitting,
    setFieldTouched,
    setFieldValue
  } = useFormik({
    initialValues: initialValues,
    validationSchema: loginValidationSchema,
    enableReinitialize: true,
    onSubmit: async payload => await onSaveIndicado(payload)
  })

  return (
    <Form onSubmit={handleSubmit}>
      <Panel>
        <PanelTitle>
          Adicionando Indicado
          <Col>
            {
              onDeleteIndicado && (
                <Button
                  type='danger'
                  onClick={() => onDeleteIndicado()}
                  style={{ marginRight: 14 }}
                >
                  Remover
                </Button>
              )
            }
            <Button type='primary' htmlType='submit' loading={isSubmitting}>
              Salvar
          </Button>
          </Col>
        </PanelTitle>
        <PanelBody>
          <Row gutter={14}>
            <Col span={12}>
              <Item
                label='Nome'
                validateStatus={
                  errors.nome && touched.nome ? 'error' : 'success'
                }
                help={touched.nome && errors.nome}
              >
                <Input
                  id='nome'
                  name='nome'
                  prefix={
                    <Icon type='user' style={{ color: 'rgba(0,0,0,.25)' }} />
                  }
                  placeholder='Digite um nome'
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.nome}
                />
              </Item>
            </Col>

            <Col span={12}>
              <Item
                label='Status Indicado'
                validateStatus={
                  errors.status && touched.status ? 'error' : 'success'
                }
                help={touched.status && errors.status}
              >
                <Select<IndicadoStatus>
                  id='status'
                  placeholder='Selecione o Status do Indicado'
                  onBlur={() => setFieldTouched('status', true)}
                  onChange={value => setFieldValue('status', value)}
                  value={values.status}
                >
                  <Option value='alternativa'>Alternativa</Option>
                  <Option value='indicado'>Indicado</Option>
                </Select>
              </Item>
            </Col>
          </Row>

          <Row gutter={14}>
            <Col span={12}>
              <Item
                label='Cargo'
                validateStatus={
                  errors.cargoId && touched.cargoId ? 'error' : 'success'
                }
                help={touched.cargoId && errors.cargoId}
              >
                <SelectCargos
                  id='cargoId'
                  setFieldTouched={setFieldTouched}
                  setFieldValue={setFieldValue}
                  value={values.cargoId}
                />
              </Item>
            </Col>
          </Row>
        </PanelBody>
      </Panel>
    </Form>
  )
}

const loginValidationSchema = yup.object().shape({
  nome: yup.string().required(),
  status: yup.string().required(),
  cargoId: yup.number().required(),
})