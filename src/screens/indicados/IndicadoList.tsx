import { Icon, Table } from 'antd'
import Column from 'antd/lib/table/Column'
import { observer } from 'mobx-react'
import React, { FunctionComponent, useContext, useEffect } from 'react'
import { useHistory, useRouteMatch } from 'react-router-dom'
import { FilterInput } from '../../components/FilterInput'
import { FilterSelect } from '../../components/FilterSelect'
import { IIndicado, IIndicadoRouteParams } from '../../interfaces/IIndicado'
import { IndicadoStoreContext } from '../../stores/IndicadoStore'
import { IIndicadoFilters } from '../../stores/IndicadoStore'
import { FilterCargos } from './FilterCargos'

export const IndicadoList: FunctionComponent = observer(() => {
  const {
    indicados,
    isLoadingIndicados,
    page,
    pageSize,
    filters,
    setFilters,
    pageConfig,
    fetchIndicados
  } = useContext(IndicadoStoreContext)
  const { push } = useHistory()
  const match = useRouteMatch<IIndicadoRouteParams>('/indicados/editando/:id')
  const currentRouteId = parseInt(match?.params.id!)

  const isRowSelected = (row: IIndicado) =>
    row.id === currentRouteId ? 'table__row--selected' : ''

  useEffect(() => {
    push('/indicados')
    fetchIndicados()
  }, [page, pageSize, filters, push, fetchIndicados])

  return (
    <Table
      rowKey='id'
      dataSource={indicados}
      loading={isLoadingIndicados}
      pagination={pageConfig}
      rowClassName={row => `table__row--clickable ${isRowSelected(row)}`}
      onRow={record => {
        return { onClick: event => push(`/indicados/editando/${record.id}`) }
      }}
    >
      <Column
        title='Nome'
        dataIndex='nome'
        filterIcon={
          <Icon
            type='search'
            style={filters.nome ? { color: '#1890ff' } : undefined}
          />
        }
        filterDropdown={() => (
          <FilterInput<keyof IIndicadoFilters>
            filterName='Nome'
            columnName='nome'
            setFilters={setFilters}
          />
        )}
      />
      <Column<IIndicado>
        title='Cargo'
        render={(_, record) =>
          `${record.cargo?.nome} - ${record.cargo?.local?.nome}`
        }
        filterIcon={
          <Icon
            type='search'
            style={filters.cargoId ? { color: '#1890ff' } : undefined}
          />
        }
        filterDropdown={() => <FilterCargos setFilters={setFilters} />}
      />
      <Column<IIndicado>
        title='Status Indicado'
        render={(_, record) =>
          record.status.charAt(0).toUpperCase() + record.status.slice(1)
        }
        filterIcon={
          <Icon
            type='search'
            style={filters.status ? { color: '#1890ff' } : undefined}
          />
        }
        filterDropdown={() => (
          <FilterSelect<keyof IIndicadoFilters>
            options={['alternativa', 'indicado']}
            filterName='Status Indicados'
            columnName='status'
            setFilters={setFilters}
          />
        )}
      />
    </Table>
  )
})
