import { Button, Col } from 'antd'
import React, { FunctionComponent } from 'react'
import {
  Link,
  Route,
  Switch,
  useHistory,
  useRouteMatch
} from 'react-router-dom'
import { Empty } from '../../components/Empty'
import {
  PageHeader,
  PageHeaderTitle,
  Panel,
  PanelTitle
} from '../../components/styled-components'
import { useAsPage } from '../../hooks/useAsPage'
import { IndicadoList } from './IndicadoList'
import { IndicadoEdit } from './IndicadoEdit'
import { IndicadoCreate } from './IndicadoCreate'

const IndicadoIndex: FunctionComponent = () => {
  useAsPage('indicados', 'Genrenciar Indicados')
  const { push } = useHistory()
  const { path } = useRouteMatch()

  return (
    <>
      <PageHeader className='page-title'>
        <PageHeaderTitle>Gerenciamento de Indicados</PageHeaderTitle>
      </PageHeader>
      <Col span={12}>
        <Panel>
          <PanelTitle>
            Indicados
            <Link to={`${path}/novo`}>
              <Button type='primary' onClick={() => push(`${path}/novo)`)}>
                Adicionar
              </Button>
            </Link>
          </PanelTitle>
          <IndicadoList />
        </Panel>
      </Col>
      <Col span={12}>
        <Switch>
          <Route
            exact
            path={path}
            component={() => <Empty name='indicado' />}
          />
          <Route path={`${path}/editando/:id`} component={IndicadoEdit} />
          <Route path={`${path}/novo`} component={IndicadoCreate} />
        </Switch>
      </Col>
    </>
  )
}

export default IndicadoIndex
