import React, { FunctionComponent } from 'react'
import { useAsPage } from '../../hooks/useAsPage'
import { PageHeader } from '../../components/styled-components'

const About: FunctionComponent = () => {
  useAsPage('sobre', 'Quem Somos')

  return (
    <PageHeader>
      <img
        alt='Quem Somos'
        src={require('../../assets/images/about-img.png')}
        style={{ maxHeight: 400 }}
      ></img>
    </PageHeader>
  )
}

export default About
