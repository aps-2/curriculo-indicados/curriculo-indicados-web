import React, { FunctionComponent, useContext } from 'react'
import { useHistory } from 'react-router-dom'
import { IUserPayload } from '../../interfaces/IUser'
import { UserStoreContext } from '../../stores/UserStore'
import { UserFormCreate } from './UserFormCreate'

export const UserCreate: FunctionComponent = () => {
  const { saveUser, fetchUsers } = useContext(UserStoreContext)
  const { push } = useHistory()
  const initialValues = {
    name: '',
    role: undefined,
    email: '',
    password: ''
  } as IUserPayload

  const onSaveUser = async (payload: IUserPayload) => {
    const { id } = await saveUser(payload)
    await fetchUsers()
    push(`/usuarios/editando/${id}`)
  }

  return (
    <UserFormCreate initialValues={initialValues} onSaveUser={onSaveUser} />
  )
}
