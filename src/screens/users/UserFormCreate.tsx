import { Button, Col, Form, Icon, Input, Row, Select } from 'antd'
import { useFormik } from 'formik'
import React, { FunctionComponent } from 'react'
import {
  Panel,
  PanelBody,
  PanelTitle
} from '../../components/styled-components'
import { IUserPayload, UserRole } from '../../interfaces/IUser'
import yup from '../../shared/yupPtBrLocale'

interface ILoginModal {
  initialValues: IUserPayload
  onSaveUser: (payload: IUserPayload) => Promise<void>
}

export const UserFormCreate: FunctionComponent<ILoginModal> = ({
  initialValues,
  onSaveUser
}) => {
  const { Item } = Form
  const { Option } = Select

  const {
    handleChange,
    handleBlur,
    handleSubmit,
    errors,
    touched,
    values,
    isSubmitting,
    setFieldTouched,
    setFieldValue
  } = useFormik({
    initialValues: initialValues,
    validationSchema: loginValidationSchema,
    onSubmit: async payload => await onSaveUser(payload)
  })

  return (
    <Form onSubmit={handleSubmit}>
      <Panel>
        <PanelTitle>
          Adicionando Usuário
          <Button type='primary' htmlType='submit' loading={isSubmitting}>
            Salvar
          </Button>
        </PanelTitle>
        <PanelBody>
          <Row gutter={14}>
            <Col span={12}>
              <Item
                label='Nome'
                validateStatus={
                  errors.name && touched.name ? 'error' : 'success'
                }
                help={touched.name && errors.name}
              >
                <Input
                  id='name'
                  name='name'
                  prefix={
                    <Icon type='user' style={{ color: 'rgba(0,0,0,.25)' }} />
                  }
                  placeholder='Digite um nome'
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.name}
                />
              </Item>
            </Col>

            <Col span={12}>
              <Item
                label='Nível de Acesso'
                validateStatus={
                  errors.role && touched.role ? 'error' : 'success'
                }
                help={touched.role && errors.role}
              >
                <Select<UserRole>
                  id='role'
                  placeholder='Selecione um Nível de Acesso'
                  onBlur={() => setFieldTouched('role', true)}
                  onChange={value => setFieldValue('role', value)}
                  value={values.role}
                >
                  <Option value='admin'>Admin</Option>
                  <Option value='mod'>Mod</Option>
                  <Option value='user'>User</Option>
                </Select>
              </Item>
            </Col>
          </Row>

          <Row gutter={14}>
            <Col span={12}>
              <Item
                label='Email'
                validateStatus={
                  errors.email && touched.email ? 'error' : 'success'
                }
                help={touched.email && errors.email}
              >
                <Input
                  id='email'
                  name='email'
                  prefix={
                    <Icon type='mail' style={{ color: 'rgba(0,0,0,.25)' }} />
                  }
                  placeholder='Email'
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.email}
                />
              </Item>
            </Col>
            <Col span={12}>
              <Item
                label='Senha'
                validateStatus={
                  errors.password && touched.password ? 'error' : 'success'
                }
                help={touched.password && errors.password}
              >
                <Input
                  id='password'
                  name='password'
                  type='password'
                  prefix={
                    <Icon type='key' style={{ color: 'rgba(0,0,0,.25)' }} />
                  }
                  placeholder='Senha'
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.password}
                />
              </Item>
            </Col>
          </Row>
        </PanelBody>
      </Panel>
    </Form>
  )
}

const loginValidationSchema = yup.object().shape({
  name: yup.string().required(),
  email: yup
    .string()
    .email()
    .required(),
  role: yup.string().required(),
  password: yup
    .string()
    .required()
    .min(8)
})
