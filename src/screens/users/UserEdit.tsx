import { Spin } from 'antd'
import { observer } from 'mobx-react'
import React, { FunctionComponent, useContext, useEffect } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { IUserPayload, IUserRouteParams } from '../../interfaces/IUser'
import { UserStoreContext } from '../../stores/UserStore'
import { UserFormEdit } from './UserFormEdit'
import { UserFormPassword } from './UserFormPassword'

export const UserEdit: FunctionComponent = observer(() => {
  const {
    user,
    saveUser,
    fetchUser,
    fetchUsers,
    isLoadingUser,
    deleteUser
  } = useContext(UserStoreContext)
  const { id } = useParams<IUserRouteParams>()
  const { push } = useHistory()
  const initialValues = {
    id: user?.id,
    name: user?.name,
    email: user?.email,
    role: user?.role
  } as IUserPayload

  const onSaveUser = async (payload: IUserPayload) => {
    await saveUser(payload)
    await fetchUsers()
  }

  const onDeleteUser = async () => {
    await deleteUser(id)
    await fetchUsers()
    push('/usuarios')
  }

  useEffect(() => {
    fetchUser(id)
  }, [id, fetchUser])

  return (
    <Spin spinning={isLoadingUser}>
      <UserFormEdit
        initialValues={initialValues}
        onSaveUser={onSaveUser}
        onDeleteUser={onDeleteUser}
      />
      <UserFormPassword userId={id} onSaveUser={onSaveUser} />
    </Spin>
  )
})
