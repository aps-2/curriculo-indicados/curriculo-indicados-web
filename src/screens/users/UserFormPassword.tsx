import { Button, Col, Form, Icon, Input, Row } from 'antd'
import { useFormik } from 'formik'
import React, { FunctionComponent, useEffect } from 'react'
import {
  Panel,
  PanelBody,
  PanelTitle
} from '../../components/styled-components'
import { IUserPayload } from '../../interfaces/IUser'
import yup from '../../shared/yupPtBrLocale'

interface ILoginModal {
  userId: string
  onSaveUser: (payload: IUserPayload) => Promise<void>
}

export const UserFormPassword: FunctionComponent<ILoginModal> = ({
  userId,
  onSaveUser
}) => {
  const { Item } = Form
  const id = parseInt(userId)

  const {
    handleChange,
    handleBlur,
    handleSubmit,
    errors,
    touched,
    values,
    isSubmitting,
    resetForm
  } = useFormik({
    initialValues: {
      password: '',
      confirmpw: ''
    },
    validationSchema: loginValidationSchema,
    enableReinitialize: true,
    onSubmit: async ({ password }) => await onSaveUser({ id, password })
  })

  useEffect(() => {
    resetForm()
  }, [id, resetForm])

  return (
    <Form onSubmit={handleSubmit}>
      <Panel>
        <PanelTitle>
          Alterando Senha
          <Col>
            <Button type='primary' htmlType='submit' loading={isSubmitting}>
              Salvar
            </Button>
          </Col>
        </PanelTitle>
        <PanelBody>
          <Row gutter={14}>
            <Col span={12}>
              <Item
                label='Senha'
                validateStatus={
                  errors.password && touched.password ? 'error' : 'success'
                }
                help={touched.password && errors.password}
              >
                <Input
                  id='password'
                  name='password'
                  type='password'
                  prefix={
                    <Icon type='key' style={{ color: 'rgba(0,0,0,.25)' }} />
                  }
                  placeholder='Digite uma senha'
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.password}
                />
              </Item>
            </Col>

            <Col span={12}>
              <Item
                label='Confirmação'
                validateStatus={
                  errors.confirmpw && touched.confirmpw ? 'error' : 'success'
                }
                help={touched.confirmpw && errors.confirmpw}
              >
                <Input
                  id='confirmpw'
                  name='confirmpw'
                  type='password'
                  prefix={
                    <Icon type='key' style={{ color: 'rgba(0,0,0,.25)' }} />
                  }
                  placeholder='Digite a confirmação'
                  onBlur={handleBlur}
                  onChange={handleChange}
                  value={values.confirmpw}
                />
              </Item>
            </Col>
          </Row>
        </PanelBody>
      </Panel>
    </Form>
  )
}

const loginValidationSchema = yup.object().shape({
  password: yup
    .string()
    .min(8)
    .required(),
  confirmpw: yup
    .string()
    .required()
    .oneOf([yup.ref('password')], 'Não coincide')
})
