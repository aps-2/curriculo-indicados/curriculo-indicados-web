import { Icon, Table } from 'antd'
import Column from 'antd/lib/table/Column'
import { observer } from 'mobx-react'
import React, { FunctionComponent, useContext, useEffect } from 'react'
import { useHistory, useRouteMatch } from 'react-router-dom'
import { FilterInput } from '../../components/FilterInput'
import { FilterSelect } from '../../components/FilterSelect'
import { IUser, IUserRouteParams } from '../../interfaces/IUser'
import { IUserFilters, UserStoreContext } from '../../stores/UserStore'

export const UserList: FunctionComponent = observer(() => {
  const {
    users,
    isLoadingUsers,
    page,
    pageSize,
    filters,
    setFilters,
    pageConfig,
    fetchUsers
  } = useContext(UserStoreContext)
  const { push } = useHistory()
  const match = useRouteMatch<IUserRouteParams>('/usuarios/editando/:id')
  const currentRouteId = parseInt(match?.params.id!)

  const isRowSelected = (row: IUser) =>
    row.id === currentRouteId ? 'table__row--selected' : ''

  useEffect(() => {
    push('/usuarios')
    fetchUsers()
  }, [page, pageSize, filters, push, fetchUsers])

  return (
    <Table
      rowKey='id'
      dataSource={users}
      loading={isLoadingUsers}
      pagination={pageConfig}
      rowClassName={row => `table__row--clickable ${isRowSelected(row)}`}
      onRow={record => {
        return { onClick: event => push(`/usuarios/editando/${record.id}`) }
      }}
    >
      <Column
        title='Nome'
        dataIndex='name'
        filterIcon={
          <Icon
            type='search'
            style={filters.name ? { color: '#1890ff' } : undefined}
          />
        }
        filterDropdown={() => (
          <FilterInput<keyof IUserFilters>
            filterName='Nome'
            columnName='name'
            setFilters={setFilters}
          />
        )}
      />
      <Column
        title='Email'
        dataIndex='email'
        filterIcon={
          <Icon
            type='search'
            style={filters.email ? { color: '#1890ff' } : undefined}
          />
        }
        filterDropdown={() => (
          <FilterInput<keyof IUserFilters>
            filterName='Email'
            columnName='email'
            setFilters={setFilters}
          />
        )}
      />
      <Column<IUser>
        title='Nível Acesso'
        render={(_, record) =>
          record.role.charAt(0).toUpperCase() + record.role.slice(1)
        }
        filterIcon={
          <Icon
            type='search'
            style={filters.role ? { color: '#1890ff' } : undefined}
          />
        }
        filterDropdown={() => (
          <FilterSelect<keyof IUserFilters>
            options={['admin', 'mod', 'user']}
            filterName='Nível de Acesso'
            columnName='role'
            setFilters={setFilters}
          />
        )}
      />
    </Table>
  )
})
