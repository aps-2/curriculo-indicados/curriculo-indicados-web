import { Row, Spin, Typography } from 'antd'
import { observer } from 'mobx-react'
import React, { FunctionComponent, useContext, useEffect } from 'react'
import { IndicadoList } from '../../components/indicado/Indicado'
import { PageHeader } from '../../components/styled-components'
import { useAsPage } from '../../hooks/useAsPage'
import { IndicadoStoreContext } from '../../stores/IndicadoStore'

const { Title } = Typography

const Home: FunctionComponent = observer(() => {
  useAsPage('home')

  const { indicados, fetchIndicados, isLoadingIndicados } = useContext(
    IndicadoStoreContext
  )

  useEffect(() => {
    fetchIndicados()
  }, [fetchIndicados])

  return (
    <>
      <PageHeader>
        <img
          alt='Pessoas felizes'
          src={require('../../assets/images/home-img.jpg')}
          style={{ maxHeight: 400 }}
        ></img>
      </PageHeader>
      <Title
        level={2}
        style={{
          fontFamily: 'Poppins Black',
          fontSize: 22,
          marginLeft: 36,
          marginTop: 10
        }}
      >
        INDICADOS
      </Title>
      <hr
        style={{
          height: 5,
          borderRadius: 20,
          borderWidth: 0,
          background: '#0084f4'
        }}
      />
      <Spin spinning={isLoadingIndicados}>
        <Row type={'flex'} justify={'space-between'}>
          {indicados?.map((i, index) => (
            <IndicadoList
              key={index}
              nome={i.nome}
              cargo={i.cargo?.nome}
              local={i.cargo?.local?.nome}
              atribuicao={i.status}
            />
          ))}
        </Row>
      </Spin>
    </>
  )
})

export default Home
